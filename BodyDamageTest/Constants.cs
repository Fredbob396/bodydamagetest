﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest
{
    class Constants
    {
        public const int PART_STATUS_RAGDOLLED = 1;
        public const int PART_STATUS_BROKEN = 2;
    }
}
