﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    class TestBody : Body
    {
        private bool _isDead { get; set; }
        public Root Root { get; set; }
        public Torso Torso { get; set; }
        public Head Head { get; set; }
        public Arm LArm { get; set; }
        public Arm RArm { get; set; }
        public Leg LLeg { get; set; }
        public Leg RLeg { get; set; }

        public TestBody()
        {
            Root = new Root();
            Torso = (Torso)Root.TorsoJoint.ChildBodyPart;
            Head = (Head)Torso.HeadJoint.ChildBodyPart;
            LArm = (Arm)Torso.LArmJoint.ChildBodyPart;
            RArm = (Arm)Torso.RArmJoint.ChildBodyPart;
            LLeg = (Leg)Root.LLegJoint.ChildBodyPart;
            RLeg = (Leg)Root.RLegJoint.ChildBodyPart;

            Parts.AddRange(new BodyPart[]
            {
                Root, Torso, Head, LArm, RArm, LLeg, RLeg
            });
        }

        public sealed override void Die()
        {
            base.Die();
            Console.WriteLine("TestBody death: Break all parts");
            Parts.ForEach(x => x.Break());
        }
    }
}
