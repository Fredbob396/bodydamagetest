﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    class Body : IBody
    {
        protected internal bool IsDead { get; private set; }
        public List<BodyPart> Parts { get; protected set; } = new List<BodyPart>();

        public Body()
        {

        }

        /// <summary>
        /// Change the health of a Body's body part.
        /// </summary>
        public void ChangePartHealth(BodyPart part, double health)
        {
            // Can't change health of part that doesn't belong to body!
            if (!Parts.Contains(part)) return;

            int partStatus = part.ChangeHealth(health);
            if(partStatus == Constants.PART_STATUS_BROKEN && part.FatalIfBroken)
            {
                Die();
            }
        }

        public virtual void Die()
        {
            // TODO: Force unbreakable parts to break?
            Console.WriteLine("Base body death");
            IsDead = true;
        }
    }
}
