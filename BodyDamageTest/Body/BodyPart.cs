﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    internal abstract class BodyPart : IBodyPart
    {
        /// <summary>
        /// The part is able to be turned into a ragdoll
        /// </summary>
        protected internal bool Ragdollable { get; protected set; } = true;
        /// <summary>
        /// The part is ragdolled
        /// </summary>
        protected internal bool Ragdolled { get; protected set; }
        /// <summary>
        /// The part is able to be broken
        /// </summary>
        protected internal bool Breakable { get; protected set; } = true;
        /// <summary>
        /// The part is broken
        /// </summary>
        protected internal bool Broken { get; protected set; }
        /// <summary>
        /// The destruction of this part results in the death of its host
        /// </summary>
        protected internal bool FatalIfBroken { get; protected set; }

        protected internal string Name { get; protected set; }
        protected internal double Health { get; protected set; } = 100.0;
        public List<Joint> Joints { get; protected set; } = new List<Joint>();

        /// <summary>
        /// Always overridden, but base should be called.
        /// </summary>
        public virtual int ChangeHealth(double health)
        {
            Health += health;

            bool broken = Health <= 0.0;
            if (broken && !Broken)
                return Constants.PART_STATUS_BROKEN;

            bool ragdolled = Health < 50.0;
            if (ragdolled && !Ragdolled)
                return Constants.PART_STATUS_RAGDOLLED;

            return 0;
        }

        /// <summary>
        /// Always overridden, but base should be called.
        /// </summary>
        public virtual void Break()
        {
            if (Broken || !Breakable) return;
            Broken = true;
            if (Joints.Count > 0)
            {
                // Joints and child BPs are destroyed before current BP
                Console.WriteLine("Base BP break: BP has joints - Destroy all joints");
                Joints.ForEach(x => x.Break());
            } 
        }

        public virtual void Ragdollize()
        {
            if (Ragdolled || !Ragdollable) return;
            Ragdolled = true;
            if(Joints.Count > 0)
            {
                // Joints and child BPs are ragdollized before current BP
                Console.WriteLine("Base BP ragdollize: BP has joints - Ragdollize all joints");
                Joints.ForEach(x => x.Ragdollize());
            }
        }
    }
}
