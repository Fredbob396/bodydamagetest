﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    interface IBody
    {
        void ChangePartHealth(BodyPart part, double health);
        void Die();
    }
}
