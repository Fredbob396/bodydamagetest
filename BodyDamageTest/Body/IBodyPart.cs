﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    interface IBodyPart
    {
        int ChangeHealth(double health);
        void Break();
        void Ragdollize();
    }
}
