﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    internal class Joint : BodyPart
    {
        /// <summary>
        /// The part higher in the body hierarchy
        /// </summary>
        public BodyPart ParentBodyPart { get; set; }
        /// <summary>
        /// The part lower in the body hierarchy
        /// </summary>
        public BodyPart ChildBodyPart { get; set; }

        public Joint(BodyPart parentBp, BodyPart childBp)
        {
            Name = "Joint";
            ParentBodyPart = parentBp;
            ChildBodyPart = childBp;
        }

        public sealed override int ChangeHealth(double health)
        {
            var bpStatus = base.ChangeHealth(health);
            switch (bpStatus)
            {
                case Constants.PART_STATUS_RAGDOLLED:
                    Ragdollize();
                    break;
                case Constants.PART_STATUS_BROKEN:
                    Break();
                    break;
            }
            return bpStatus;
        }

        public sealed override void Break()
        {
            if (Broken || !Breakable) return;
            base.Break();
            Console.WriteLine("Joint break: Break child BP");
            ChildBodyPart.Break();
        }

        public sealed override void Ragdollize()
        {
            if (Ragdolled || !Ragdollable) return;
            base.Ragdollize();
            Console.WriteLine("Joint ragdollize: Ragdollize child BP");
            ChildBodyPart.Ragdollize();
        }
    }
}
