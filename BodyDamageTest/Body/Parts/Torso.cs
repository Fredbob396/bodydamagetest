﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    internal class Torso : BodyPart
    {
        public Joint HeadJoint {get; private set;}
        public Joint LArmJoint {get; private set;}
        public Joint RArmJoint { get; private set; }

        public Torso()
        {
            Name = "Torso";
            FatalIfBroken = true;
            HeadJoint = new Joint(this, new Head());
            LArmJoint = new Joint(this, new Arm());
            RArmJoint = new Joint(this, new Arm());
            Joints.AddRange(new []{HeadJoint, LArmJoint, RArmJoint});
        }

        public sealed override int ChangeHealth(double health)
        {
            var bpStatus = base.ChangeHealth(health);
            switch (bpStatus)
            {
                case Constants.PART_STATUS_RAGDOLLED:
                    Ragdollize();
                    break;
                case Constants.PART_STATUS_BROKEN:
                    Break();
                    break;
            }
            return bpStatus;
        }

        public sealed override void Break()
        {
            if (Broken || !Breakable) return;
            base.Break();
            Console.WriteLine("Torso break: No unique effect");
        }

        public sealed override void Ragdollize()
        {
            if (Ragdolled || !Breakable) return;
            base.Ragdollize();
            Console.WriteLine("Torso ragdollize: No unique effect");
        }
    }
}
