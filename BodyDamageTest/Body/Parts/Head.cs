﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    internal class Head : BodyPart
    {
        public Head()
        {
            Name = "Head";
            FatalIfBroken = true;
        }

        public sealed override int ChangeHealth(double health)
        {
            var bpStatus = base.ChangeHealth(health);
            switch (bpStatus)
            {
                case Constants.PART_STATUS_RAGDOLLED:
                    Ragdollize();
                    break;
                case Constants.PART_STATUS_BROKEN:
                    Break();
                    break;
            }
            return bpStatus;
        }

        public sealed override void Break()
        {
            if (Broken || !Breakable) return;
            base.Break();
            Console.WriteLine("Head break: No unique effect");
        }

        public sealed override void Ragdollize()
        {
            if (Ragdolled || !Ragdollable) return;
            base.Ragdollize();
            Console.WriteLine("Head ragdollize: No unique effect");
        }
    }
}
