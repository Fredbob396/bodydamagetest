﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BodyDamageTest.Body
{
    internal class Root : BodyPart
    {
        public Joint TorsoJoint { get; private set; }
        public Joint LLegJoint { get; private set; }
        public Joint RLegJoint { get; private set; }

        public Root()
        {
            Name = "Pelvis";
            Ragdollable = false;
            FatalIfBroken = true;
            TorsoJoint = new Joint(this, new Torso());
            LLegJoint = new Joint(this, new Leg());
            RLegJoint = new Joint(this, new Leg());
            Joints.AddRange(new[] { TorsoJoint, LLegJoint, RLegJoint });
        }

        public sealed override int ChangeHealth(double health)
        {
            var bpStatus = base.ChangeHealth(health);
            if (bpStatus == Constants.PART_STATUS_BROKEN) Break();
            return bpStatus;
        }

        public sealed override void Break()
        {
            if (Broken || !Breakable) return;
            base.Break();
            Console.WriteLine("Root break: No unique effect");
        }
    }
}
