﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BodyDamageTest.Body;

namespace BodyDamageTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("### BODY TESTING ###");

            var body = new TestBody();

            // Should be false
            Console.WriteLine(body.IsDead);

            /*  Damage simulation
                When implemented, the BodyPart would be passed into
                this function from a unity object which is linked to
                the body part by a dictionary (probably).
            */
            body.ChangePartHealth(body.Torso, -75);

            // Should be false
            Console.WriteLine(body.IsDead);

            // Kill
            body.ChangePartHealth(body.Head, -500);

            // Should be true
            Console.WriteLine(body.IsDead);

            Console.ReadKey();
        }
    }
}
